"use strict"

const allButtons = document.querySelectorAll('.btn');

document.onkeydown = function (event){
const keyButton = event.key;
allButtons.forEach((button) => {
if (button.textContent.toLowerCase() === keyButton.toLowerCase()){
    button.style.backgroundColor = "blue";
}
else {
    button.style.backgroundColor = "black";
}
})
}